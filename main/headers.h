/*
 * headers.h
 *
 *  Created on: 9 ���. 2020 �.
 *      Author: prg-46
 */

#ifndef MAIN_HEADERS_H_
#define MAIN_HEADERS_H_

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "lwip/err.h"
#include "lwip/sys.h"

typedef struct {
	wifi_sta_list_t sta_list;
	tcpip_adapter_sta_list_t listStaAp;

} mesh_list_t;


#endif /* MAIN_HEADERS_H_ */
