#include "wifi_ap_sta.h"
#include "wifi_scan.h"
#include "get_request.h"
#include "tcp_socket.h"
#include "headers.h"


#include "string.h"

#include "lwip/sockets.h"

#define MAIN_TAG "main_tag"
const int IPV4_GOTIP_BIT = BIT0;

void app_main() {
	esp_err_t ret = nvs_flash_init();
//	if (ret == ESP_ERR_NVS_NO_FREE_PAGES
//			|| ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
//	}
	ESP_ERROR_CHECK(ret);

	ESP_LOGI(MAIN_TAG, "ESP_WIFI_MODE_STA");

	wifi_init_softap();
	vTaskDelay(10);
	xTaskCreate(tcp_server_task, "tcp_server", 4096, NULL, 5, NULL);

	wifi_init_sta();
	app_wifi_wait_connected(); 	// ���� ���������� � STA

	//��� ���������� ������� ����� ����� ����������� � ���������
//	xTaskCreatePinnedToCore(check_inet_connect, "check_inet_connect", 1024 * 6, NULL, 4, NULL, 1);
	init_socket_tcp();


}
