 #include "wifi_ap_sta.h"
#include "wifi_scan.h"
#include "..\tcp_socket\include\tcp_socket.h"
#include "bson.h"

#define WIFI_AP_STA_TAG "WIFI_ap_sta"


static int s_retry_num = 0;
static bool got_scan_done_event = false;
static EventGroupHandle_t s_wifi_event_group;
const int WIFI_CONNECTED_BIT = BIT0;


mesh_list_t meshList;


void app_wifi_wait_connected() {
	xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT, false, true,
			portMAX_DELAY);
}

void change_IP(int val_ip){
	uint8_t ap_ip[4];
	ap_ip[0]=192;
	ap_ip[1]=168;
	ap_ip[2]=val_ip;
	ap_ip[3]=1;
	tcpip_adapter_ip_info_t ip_info;
	IP4_ADDR(&ip_info.ip, ap_ip[0], ap_ip[1], ap_ip[2], ap_ip[3]);
	IP4_ADDR(&ip_info.gw, ap_ip[0], ap_ip[1], ap_ip[2], ap_ip[3]);
	IP4_ADDR(&ip_info.netmask, 255, 255, 255, 0);
	uint8_t ap_mac[6];
	esp_wifi_get_mac(ESP_IF_WIFI_AP, ap_mac);
	tcpip_adapter_ap_start(ap_mac, &ip_info);
}



void listPrint(mesh_list_t *l) {
	tcpip_adapter_sta_list_t lisIP;
	vTaskDelay(100);
	esp_wifi_ap_get_sta_list(&l->sta_list);
	tcpip_adapter_get_sta_list(&l->sta_list, &l->listStaAp);
//	for(int i=0;i<l->listStaAp.num;i++){
//		printf(MACSTR, MAC2STR(l->listStaAp.sta[i].mac));
//		printf("IP=\n");
//		printf("%s",ip4addr_ntoa(&(l->listStaAp.sta[i].ip)));
//		printf("\n");
//	}
}


static esp_err_t event_handler(void *ctx, system_event_t *event) {
	switch (event->event_id) {
	case SYSTEM_EVENT_STA_START:
		esp_wifi_connect();
		break;
	case SYSTEM_EVENT_STA_GOT_IP:
		ESP_LOGI(WIFI_AP_STA_TAG, "got ip:%s",
				esp_got_ip = ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
		s_retry_num = 0;
		xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);

		break;
	case SYSTEM_EVENT_STA_DISCONNECTED: {
		if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) {
			esp_wifi_connect();
			xEventGroupClearBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
			s_retry_num++;
			ESP_LOGI(WIFI_AP_STA_TAG, "retry to connect to the AP");
		}
		ESP_LOGI(WIFI_AP_STA_TAG, "connect to the AP fail\n");
		break;
	}
	case SYSTEM_EVENT_AP_START: {
		ESP_LOGI(WIFI_AP_STA_TAG, "SYSTEM_EVENT_AP_START");
	}
		break;

	case SYSTEM_EVENT_AP_STACONNECTED:{
		ESP_LOGI(WIFI_AP_STA_TAG, "station:"MACSTR" join, AID=%d",
				MAC2STR(event->event_info.sta_connected.mac),
				event->event_info.sta_connected.aid);
		listPrint(&meshList);
		sendToBson(&meshList);
	}
		break;

	case SYSTEM_EVENT_AP_STADISCONNECTED:
		ESP_LOGI(WIFI_AP_STA_TAG, "station:"MACSTR"leave, AID=%d",
				MAC2STR(event->event_info.sta_disconnected.mac),
				event->event_info.sta_disconnected.aid);
		break;
	case SYSTEM_EVENT_SCAN_DONE:
		got_scan_done_event = true;
		break;

	default:
		break;
	}
	return ESP_OK;
}


void wifi_init_sta() {

	s_wifi_event_group = xEventGroupCreate();
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
	ESP_ERROR_CHECK(esp_wifi_start());
	vTaskDelay(10);
	ESP_ERROR_CHECK(esp_wifi_scan_start(NULL, true));
	wifi_ap_record_t ap_info[20];
	while (1) {
		if (got_scan_done_event == true) {
			perform_scan(ap_info);
			got_scan_done_event = false;
			break;
		} else {
			vTaskDelay(100 / portTICK_RATE_MS);
		}
	}

	int8_t check_i = -1;
	for (int i = 0, check_rssi = 999; i < 20; i++) { // �����  ������� ����������
		if (strstr((const char*) ap_info[i].ssid, MESH_NAME) > NULL) {
			if (ap_info[i].rssi < check_rssi) {
				check_rssi = ap_info[i].rssi;
				check_i = i;
			}
		}
	}
	esp_wifi_scan_stop();
	wifi_config_t wifi_config_sta = { .sta = {
			.ssid = "****",
			.password = "****" }
	};
	if (check_i > -1) {
		memcpy(wifi_config_sta.sta.ssid, ap_info[check_i].ssid,
				sizeof(ap_info[check_i].ssid));
		memcpy(wifi_config_sta.sta.password, STA_PASS, sizeof(STA_PASS));
		esp_wifi_stop();
		ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config_sta));
		change_IP(1);
		ESP_ERROR_CHECK(esp_wifi_start());
		ESP_LOGI(WIFI_AP_STA_TAG, "wifi_init_sta finished.");
		ESP_LOGI(WIFI_AP_STA_TAG, "connect to ap SSID:%s password:%s",
				wifi_config_sta.sta.ssid, wifi_config_sta.sta.password);
	}

}






void wifi_init_softap() {

	s_wifi_event_group = xEventGroupCreate();

	tcpip_adapter_init();
	ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	wifi_config_t wifi_config_ap = {
		.ap = {
			.ssid = AP_SSID,
			.ssid_len = strlen(AP_SSID),
			.password = AP_PASS,
			.max_connection = AP_STA_CONN,
//					.ssid_hidden = 1,			//����� ��������
			.authmode = WIFI_AUTH_WPA_WPA2_PSK },
	};

	if (strlen(AP_PASS) == 0) {
		wifi_config_ap.ap.authmode = WIFI_AUTH_OPEN;
	}

	change_IP(1);

	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config_ap));
	ESP_ERROR_CHECK(esp_wifi_start());

	ESP_LOGI(WIFI_AP_STA_TAG, "wifi_init_softap finished.SSID:%s password:%s",
			AP_SSID, AP_PASS);

}
