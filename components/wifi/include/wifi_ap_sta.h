/*
 * wifi_ap_sta.h
 *
 *  Created on: 30 ����. 2020 �.
 *      Author: prg-46
 */

#ifndef COMPONENTS_WIFI_INCLUDE_WIFI_AP_STA_H_
#define COMPONENTS_WIFI_INCLUDE_WIFI_AP_STA_H_

#define EXAMPLE_ESP_MAXIMUM_RETRY  5
#define AP_SSID      "MESH__1"

#define AP_PASS      "qwertyuiop"
#define STA_PASS      "metall-opt-torg"
//#define STA_PASS      "qwertyuiop"


#define AP_STA_CONN       5

static const char MESH_NAME[] = "metall-2g";
//static const char MESH_NAME[] = "MESH__";

const char *esp_got_ip;

void wifi_init_softap();
void wifi_init_sta();

#endif /* COMPONENTS_WIFI_INCLUDE_WIFI_AP_STA_H_ */
