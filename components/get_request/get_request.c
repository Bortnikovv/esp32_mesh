
#include "get_request.h"

#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"
//#include "app_wifi.h"

#include "esp_http_client.h"

static const char *TAG_REQUEST = "TAG_REQUEST";

extern const char howsmyssl_com_root_cert_pem_start[] asm("_binary_howsmyssl_com_root_cert_pem_start");
extern const char howsmyssl_com_root_cert_pem_end[]   asm("_binary_howsmyssl_com_root_cert_pem_end");

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGD(TAG_REQUEST, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGD(TAG_REQUEST, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGD(TAG_REQUEST, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGD(TAG_REQUEST, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGD(TAG_REQUEST, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            if (!esp_http_client_is_chunked_response(evt->client)) {
                // Write out data
                // printf("%.*s", evt->data_len, (char*)evt->data);
            }

            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGD(TAG_REQUEST, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGD(TAG_REQUEST, "HTTP_EVENT_DISCONNECTED");
            break;
    }
    return ESP_OK;
}



esp_err_t http_rest_with_url()
{
    esp_http_client_config_t config = {
        .url = "http://httpbin.org/get",
        .event_handler = _http_event_handler,
		.timeout_ms = 3000,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);

    // GET
    esp_err_t err = esp_http_client_perform(client);
//    if (err == ESP_OK) {
//        ESP_LOGI(TAG_REQUEST, "HTTP GET Status = %d, content_length = %d",
//                esp_http_client_get_status_code(client),
//                esp_http_client_get_content_length(client));
//    } else {
//        ESP_LOGE(TAG_REQUEST, "HTTP GET request failed: %s", esp_err_to_name(err));
//    }
    esp_http_client_cleanup(client);
    return err;
}


void check_inet_connect(void *pvParameter){
	bool check_tcp = false;
	while(1){
		if(http_rest_with_url() != ESP_OK){
			ESP_LOGE(TAG_REQUEST,"No internet connection");
		}
		vTaskDelay(10000 / portTICK_PERIOD_MS);
	}
	vTaskDelete(NULL);
}
