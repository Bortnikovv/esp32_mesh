
#include <stdlib.h>
#include "bson.h"
#include "include\lib_minibson.hpp"
#include <stdio.h>
#include "lwip/sockets.h"


void read(minibson::document &m)
{
    std::cout<<std::endl <<"myMAC= "<< m.get("mac","NULL")<< std::endl;
    int qCh = m.get("qCh",0);

    for(int i=1;i<=qCh; i++){
		char p[2];
		itoa(i,p,10);
		std::cout<<"p=" <<p << std::endl;
		auto docRead = m.get(p,minibson::document());
		std::cout << docRead.get("mac","NULL") << std::endl;
    }
}


void convertMinibison(char * buf,int s){
	char *ptr =buf+2;
	minibson::document m(ptr,s-2);
	read(m);
}


void sendToBson(mesh_list_t *m_list){

	minibson::document mesh;
	mesh.set("mac","myMac");
	mesh.set("qCh",m_list->listStaAp.num);
	for (int i=0; i<m_list->listStaAp.num; i++){
		minibson::document child;

		char num[2]="";
		itoa(i+1, num, 10);
		char  mac[20];
		sprintf(mac,MACSTR, MAC2STR(m_list->listStaAp.sta[i].mac));

		child.set<std::string>("mac", mac);
		std::cout<<"a = "<<child.get("mac", "")<<std::endl;
		child.set("qCh", 0);
		mesh.set(num, child);
	}

    size_t size = mesh.get_serialized_size();
    char* buffer = new char[size+2];
    buffer[0] = 'B';
    buffer[1] = '/';
    char* ptr = buffer + 2;
    mesh.serialize(ptr, size);


//    int err = send(55, buffer, size+2, 0);
//	convertMinibison(buffer,size+2);
	    delete buffer;

}







