/*
 * tcp_socket.h
 *
 *  Created on: 2 ���. 2020 �.
 *      Author: prg-46
 */

#ifndef COMPONENTS_TCP_SOCKET_INCLUDE_TCP_SOCKET_H_
#define COMPONENTS_TCP_SOCKET_INCLUDE_TCP_SOCKET_H_

void init_socket_tcp();
void tcp_server_task(void *pvParameters);


#endif /* COMPONENTS_TCP_SOCKET_INCLUDE_TCP_SOCKET_H_ */
