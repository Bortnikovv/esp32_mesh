#include "C:\msys32\home\prg-46\esp\eclipse-workspace\mesh_esp32\components\tcp_socket\include\tcp_socket.h"
#include "C:\msys32\home\prg-46\esp\eclipse-workspace\mesh_esp32\main\headers.h"
#include "wifi_ap_sta.h"
#include "bson.h"

#include "lwip/sockets.h"
#include "esp_log.h"
#include <string.h>

#define TCP_SOCKET_TAG_S "tcp_socket_sever"
#define TCP_SOCKET_TAG_C "tcp_socket_client"
#define HOST_IP_ADDR "192.168.10.77"
#define PORT 9996

typedef struct {

	struct sockaddr_in6 sourceAddr;
	int sock;

} ip_sock_s;

#include "lwip/netif.h"



void init_socket_tcp(){			//client
	int sock;
	char addr_str[256];
	int addr_family;
	int ip_protocol;

	struct sockaddr_in destAddr;
	destAddr.sin_addr.s_addr = inet_addr(HOST_IP_ADDR);
	printf("esp_got_ip ==================%s",esp_got_ip);
	destAddr.sin_family = AF_INET;
	destAddr.sin_port = htons(PORT);
	addr_family = AF_INET;
	ip_protocol = IPPROTO_IP;
	inet_ntoa_r(destAddr.sin_addr, addr_str, sizeof(addr_str) - 1);

	sock = socket(addr_family, SOCK_STREAM, ip_protocol);
	if (sock < 0) {
		ESP_LOGE(TCP_SOCKET_TAG_C, "Unable to create socket: errno %d", errno);
		return;
	}
	ESP_LOGI(TCP_SOCKET_TAG_C, "Socket created");

	int err = connect(sock, (struct sockaddr*) &destAddr, sizeof(destAddr));
	if (err != 0) {
		ESP_LOGE(TCP_SOCKET_TAG_C, "Socket unable to connect: errno %d", errno);
		return;
	}
	ESP_LOGI(TCP_SOCKET_TAG_C, "Successfully connected");

	printf("client sock=%d\n", sock);
	char data[]="hi!!!!!!!!!!!!!";

	if (send(sock, data, strlen(data), 0) < 0)
		ESP_LOGE(TCP_SOCKET_TAG_C, "Error occured during sending: errno %d", errno);
}


void task_recv_socet(void *pvParam) {
	ip_sock_s addr = ((ip_sock_s*) pvParam)[0];
	free(pvParam);
	char addr_str[128];
	char rx_buffer[128];

	printf("tasl==%d\n", addr.sourceAddr.sin6_len);
	printf("task sock=%d\n", addr.sock);

	while (1) {
		ESP_LOGI(TCP_SOCKET_TAG_S, "[APP] Free memory: %d bytes", esp_get_free_heap_size());

		int len = recv(addr.sock, rx_buffer, sizeof(rx_buffer) - 1, 0);
		if (len < 0) {
			ESP_LOGE(TCP_SOCKET_TAG_S, "recv failed: errno %d", errno);
			break;
		} else if (len == 0) {
			ESP_LOGI(TCP_SOCKET_TAG_S, "Connection closed");
			break;
		} else {
			if (addr.sourceAddr.sin6_family == PF_INET) {
				inet_ntoa_r(
						((struct sockaddr_in* )&addr.sourceAddr)->sin_addr.s_addr,
						addr_str, sizeof(addr_str) - 1);
			} else if (addr.sourceAddr.sin6_family == PF_INET6) {
				inet6_ntoa_r(addr.sourceAddr.sin6_addr, addr_str,
						sizeof(addr_str) - 1);
			}

			rx_buffer[len] = 0; // Null-terminate whatever we received and treat like a string
			ESP_LOGI(TCP_SOCKET_TAG_S, "Received %d bytes from %s:", len, addr_str);
			ESP_LOGI(TCP_SOCKET_TAG_S, "%s", rx_buffer);
			if(rx_buffer[0] == 'B' && rx_buffer[1] == '/'  )
				convertMinibison(rx_buffer, len);
			else {
				int err = send(addr.sock, "ok", 2, 0);
				if (err < 0) {
					ESP_LOGE(TCP_SOCKET_TAG_S, "Error occured during sending: errno %d", errno);
					break;
				}
			}
		}
	}
	if (addr.sock != -1) {
		ESP_LOGE(TCP_SOCKET_TAG_S, "Shutting down socket and restarting...");
		shutdown(addr.sock, 0);
		close(addr.sock);
	}
	vTaskDelete(NULL);
}





void tcp_server_task(void *pvParameters) {
	char rx_buffer[128];
	char addr_str[128];
	int addr_family;
	int ip_protocol;

	while (1) {
		struct sockaddr_in destAddr;
		destAddr.sin_addr.s_addr = htonl(INADDR_ANY);
		destAddr.sin_family = AF_INET;
		destAddr.sin_port = htons(PORT);
		addr_family = AF_INET;
		ip_protocol = IPPROTO_IP;
		inet_ntoa_r(destAddr.sin_addr, addr_str, sizeof(addr_str) - 1);

		int listen_sock = socket(addr_family, SOCK_STREAM, ip_protocol);
		if (listen_sock < 0) {
			ESP_LOGE(TCP_SOCKET_TAG_S, "Unable to create socket: errno %d", errno);
			break;
		}
		ESP_LOGI(TCP_SOCKET_TAG_S, "Socket created");

		int err = bind(listen_sock, (struct sockaddr*) &destAddr,
				sizeof(destAddr));
		if (err != 0) {
			ESP_LOGE(TCP_SOCKET_TAG_S, "Socket unable to bind: errno %d", errno);
			break;
		}
		ESP_LOGI(TCP_SOCKET_TAG_S, "Socket binded");

		err = listen(listen_sock, 2);
		if (err != 0) {
			ESP_LOGE(TCP_SOCKET_TAG_S, "Error occured during listen: errno %d", errno);
			break;
		}
		ESP_LOGI(TCP_SOCKET_TAG_S, "Socket listening");

		while (1) {

			ip_sock_s ipSock;
			uint addrLen = sizeof(ipSock.sourceAddr);
			ipSock.sock = accept(listen_sock,
					(struct sockaddr*) &ipSock.sourceAddr, &addrLen);
			if (ipSock.sock < 0) {
				ESP_LOGE(TCP_SOCKET_TAG_S, "Unable to accept connection: errno %d", errno);
				break;
			}
			ESP_LOGI(TCP_SOCKET_TAG_S, "Socket accepted");

			void *add = malloc(sizeof(ip_sock_s));
			((ip_sock_s*) add)[0] = ipSock;

			xTaskCreate(&task_recv_socet, "task_recv_socet", 1024 * 3, add, 5, NULL);
		}
	}
	vTaskDelete(NULL);
}

